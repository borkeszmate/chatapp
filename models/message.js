const fs = require('fs');
const path = require('path');

const p = path.join(__dirname,'..','data', 'messages.json');

exports.saveMessages = (msg) => {

	fs.readFile(p, (err, fileContent) => {
		if (err) {
			console.log(err);
			return false
		}
		if (Object.keys(fileContent).length === 0) {
			msg = JSON.stringify([msg]);
			fs.writeFile(p, msg, (err) => {
				err ? console.log(err) : '';
			});

		} else {
			fs.readFile(p, 'utf8', (err, data) => {
				err ? console.log(err) : '';
				data = JSON.parse(data);
				data = [...data, msg];
				console.log(data);
				fs.writeFile(p, JSON.stringify(data), (err) => {
					if (err) throw err;
				});
			});
		};
	});
}

exports.getMessages = (cb) => {

	fs.readFile(p, (err, fileContent) => {
		if(err || fileContent === undefined) {
			console.log(err);
			return false;
		}
		cb(JSON.parse(fileContent));
	});
}
