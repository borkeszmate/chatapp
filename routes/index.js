var express = require('express');
var router = express.Router();
const home = require('../controllers/home');

/* GET home page. */
router.get('/', home.renderHome);

module.exports = router;

