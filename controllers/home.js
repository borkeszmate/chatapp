var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
const Message = require('../models/message');

exports.renderHome = (req, res, next) => {


	Message.getMessages((messages) => {
		res.render('index', {
			title: 'Socket.io mini chatapp with Express & EJS',
			messages: messages
		});
	});


};

