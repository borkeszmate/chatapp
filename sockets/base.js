const Message = require('../models/message.js');

module.exports = (io, res) => {
	io.on('connection', (socket) => {

		console.log('an user connected');

		socket.on('disconnect', () => {
			console.log('user disconnected');
		})
		socket.on('chat message', (msg) => {
			Message.saveMessages(msg, products => console.log('Mentve'));
			socket.broadcast.emit('chat message', msg);
		});
	});
}
