const socket = io();

let isLoggedin = false;
let user;


socket.on('chat message', (data) => {
	appendNewMsg(data);
});

const submitBtn = document.querySelector('#submit');
const chat = document.querySelector('.chat');

submitBtn.addEventListener('click', (e) => {
	e.preventDefault();
	const userName = document.querySelector('#username').value;
	userName !== '' ? isLoggedin = true : alert('Add a username!');
	user = userName;
	isLoggedin ? loginSubmit() : false;
});



function loginSubmit() {
	document.querySelector('.login').style.display = 'none';
	chat.style.display = 'block';
	const messageBtn = document.querySelector('#messageBtn');
	messageBtn.addEventListener('click', onMessageSend);
}

function onMessageSend() {
	let message = document.querySelector('#message').value;
	socket.emit('chat message', { 'user': user , 'message': message });
	const data = { 'user': user , 'message': message };
	appendNewMsg(data);
	message.value = '';
}

function appendNewMsg(data) {
	const container = document.querySelector('.chat__messages');
	const msg = document.createElement('div');
	msg.classList.add('chat__messages__msg');

	const h3 = document.createElement('h3');
	h3.innerHTML = data.user;
	const p = document.createElement('p');
	p.innerHTML = data.message;
	msg.appendChild(h3);
	msg.appendChild(p);
	container.appendChild(msg);
}




